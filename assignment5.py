#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]


@app.route('/book/JSON/')
def bookjson():
    return jsonify(books)


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        global books
        num = len(books)
        for i in range(num):
            if(int(books[i]['id'])  > (i+1)):
                break
        if(i == num - 1) and (int(books[i]['id']) == (i+1)):
            i = num
        name = request.form['name']
        newdata = {'title': name, 'id': str(i + 1)}
        books.insert(i, newdata)
    return render_template('newBook.html')
    # <your code>

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        global books
        name = request.form['name']
        for i in books:
            if(i['id'] == str(book_id)):
                i['title'] = name
    return render_template('editBook.html', id = book_id)
    # <your code>
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        global books
        for i in books:
            if(i['id'] == str(book_id)):
                books.remove(i)
        return render_template('showBook.html', books = books)
    return render_template('deleteBook.html', id = book_id, id_str = str(book_id),books = books)
    # <your code>


if __name__ == '__main__':
	app.debug = True
	app.run(host = '127.0.0.1', port = 5000)
	

